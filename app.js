//ToDo: 
// fullscreen

//sets default settings
var increments = null,
    readIndex = 0,
    stonks = [],
    settings = {
        speed: 225,
        align: "center",
        delay: 3,
        fullscreen: false,
    };


function prepare(text) {
    stonks = [];

    var lines = text.split('\n'),
        words = [];
    for(i = 0; i < lines.length; i++) {
        var lineWords = lines[i].split(' ');
        for(i2 = 0; i2 < lineWords.length; i2++) {
            if(lineWords[i2] !== '') {
                var trimmedWord = $('<div />').text($.trim(lineWords[i2])).html();
                words.push(trimmedWord);
            }
        }
    }
    var combined = false,
        dotPattern = /.*\./;
    for(i = 0; i < words.length; i++) {
        if(word !== '') {
            var didSentenceEnd = false;
            if(words[i].match(dotPattern)) {
                didSentenceEnd = true;
            }
            if(settings.combine && words[i].length <= 3 && !combined && i > 0) {
                var index = stonks.length - 1;
                stonks[index].text += ' ' + words[i];
                stonks[index].sentenceEnd = didSentenceEnd;
                combined = true;
            } else {
                stonks.push({text: words[i], sentenceEnd: didSentenceEnd});
                combined = false;
            }
        }
    }
    $('#text-info').html(words.length + ' words').show();
    $('body').data('prepared', true);
}


function start() {
    if(stonks.length === 0) {
        return false;
    }
    interval = 1000 / (settings.speed / 60);
    increments = window.setInterval(flashWords, interval, stonks);
    $('#start').html('Pause');
    $('body').data('reading', true);
    window.addEventListener("keydown", function(e) {
        // space and arrow keys
        if([37, 38, 39, 40].indexOf(e) > -1) {
            e.preventDefault();
        }
    }, false);
}


function stop() {
    window.clearInterval(increments);
    $('#start').html('resume');
    $('body').data('reading', false);
}


function flashWords(array) {
    var chunk = array[readIndex],
        length = array.length;
    if(readIndex == length) {
        stop();
        readIndex = 0;
    } else {
        readIndex++;
    }
    $('#word').html(chunk.text);
    $('#progress').attr({
        'max': stonks.length,
        'value': readIndex,
        'step': 1
    }).show();
}


function savesettings() {
    localStorage.setItem('settings', JSON.stringify(settings));
}


var elem = document.getElementById("reading-screen");
function openFullscreen() {
  if (elem.requestFullscreen) {
    elem.requestFullscreen();
  } else if (elem.webkitRequestFullscreen) { /* Safari */
    elem.webkitRequestFullscreen();
  }
}


$(document).ready(function() {
    if(localStorage.getItem('settings') === null) {
        localStorage.setItem('settings', JSON.stringify(settings));
    } else {
        settings = JSON.parse(localStorage.getItem('settings'));
    }
    $('body').data({'reading': false, 'prepared': false});
    prepare($('#readable').val());


    $('#start').on('click', function() {
        console.log(stonks)
        var data = $('body').data();
        if(data.reading === false) {
            if(data.prepared === false) {
                var text = $('#readable').val();
                if(text.length > 1 && settings.speed > 0) {
                    prepare(text);
                } else {
                    alert('No text to read (or invalid speed settings)');
                }
            }
            $('#readable').fadeOut(250, function() {
                if(settings.fullscreen){
                    openFullscreen();
                    var longestWordCounter = 0;
                    var longestWord = "";
                    for(var i = 0; i < stonks.length; i++){
                        if(stonks[i].text.length > longestWordCounter){
                            longestWordCounter = stonks[i].text.length;
                            longestWord = stonks[i].text;
                        }
                    }
                    // set longest word, set font same as bg, resize the font, and set color and word back
                    $('#word').html(longestWord);
                    $('#word').css( 'color', '#ffec99' );
                    $('#word').css( 'font-size', '50vh' );
                    $('#word').html($('#readable').val().split(" ")[0]);
                    $('#word').css( 'color', 'black' );
                }
                var startDelay = settings.delay * 1000;
                setTimeout(() => { start(); }, startDelay);
                $('#reading-screen, #new, #restart').fadeIn(250);
            });
            $('h1').animate({height: 0, opacity: 0}, 500);
            $('#other').fadeOut(500);
            $('#chrome').fadeOut(500);
            $('#random-article').fadeOut(500);
            $('#combine').attr('disabled', true).parent().css('opacity', 0.5);
        } else {
            stop();
        }
    });

    $('#restart').on('click', function() {
        stop();
        readIndex = 0;
        $('#progress').val(readIndex);
        $('#word').html($('#readable').val().split(" ")[0]);
    });
    
    $('#text').on('click', function() {
        settings.align = $(this).val();
        savesettings();
        if($('body').data('reading') === true) {
            stop();
            start();
        }
        
        // console.log($('#word').css( 'text-align' ));
        if ($('#word').css( 'text-align' ) ==  'left') {
            $('#word').css( 'text-align', 'center' );
        } else {
            $('#word').css( 'text-align', 'left' );
        }
    });

    $('#readable').on('keyup', function() {
        readIndex = 0;
        prepare($(this).val())
    });

    $('#reading-speed').on('change', function() {
        settings.speed = parseInt($(this).val());
        savesettings();
        if($('body').data('reading') === true) {
            stop();
            start();
        }
    });

    $('#reading-delay').on('change', function() {
        settings.delay = parseInt($(this).val());
        savesettings();
    });

    $('#reading-fullscreen').on('change', function() {
        settings.fullscreen = $(this).prop('checked');
        savesettings();
    });

    $('#progress').on('mousedown touchstart', function() {
        if($('body').data('reading')) {
            stop();
            $('body').data('reading', 'paused');
        }
        $(this).on('mousemove touchmove', function() {
            var newIndex = $(this).val();
            $('#word').html(stonks[newIndex].text);

        });
    }).on('mouseup touchend', function() {
        readIndex = $(this).val();
        $(this).off('mousemove');
        if($('body').data('reading') === 'paused') {
            start();
        }
    });
    //key controls
    $('body').on('keyup', function(e) {
        // console.log(e.keyCode)
        if($('#readable').is(':focus')) {
            return false;
        }
        //spacebar will pause/play
        if(e.keyCode == 32) {
            if($('body').data('reading') === true) {
                stop();
            } else {
                start();
            }
        //up arrow increases wpm by 5
        } else if(e.keyCode == 38) {
            $('#reading-speed').val(settings.speed + 5).trigger('change');
        //down arrow decreases wpm by 5
        } else if(e.keyCode == 40) {
            $('#reading-speed').val(settings.speed - 5).trigger('change');
        //backspace left arrow, goes back 7 words
        } else if(e.keyCode == 8 || e.keyCode == 37) {
            if($('body').data().reading === true) {
                if(readIndex < 7) {
                    readIndex = 0;
                } else {
                    readIndex = readIndex - 7;
                }
                $('#word').html(chunks[readIndex].text);
                $('#progress').val(readIndex);
            }

        }
    });

    $('#new').on('click', function() {
        $('#reading-screen').hide();
        $('#restart').fadeOut();
        $('#readable').fadeIn(500);
        $('h1').animate({height: '26px', opacity: 1}, 500);
        $(this).fadeOut(500);
        if($('body').data('reading') === true) {
            stop();
        }
    })
});
